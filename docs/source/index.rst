.. sphinx-ci-test documentation master file, created by
   sphinx-quickstart on Mon Dec 18 12:14:25 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to sphinx-ci-test's documentation!
==========================================

This project illustrates the use of `to-be-continuous CI template <https://gitlab.com/to-be-continuous>`_
for building documentation with `Sphinx <https://www.sphinx-doc.org/>`_.

.. toctree::
   :maxdepth: 2
   :caption: Contents:



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
